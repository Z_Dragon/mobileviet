<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product Detail</title>
    </head>
    <body>
        <jsp:include page="/WEB-INF/views/_header.jsp"></jsp:include>
        <div class="container">            
            <div class="row">
                <div class="col-md-4 md-col mb-4">
                    <div class="col-md">
                        <img  src="images/products/${product.prodImage}" alt="${product.prodName}" />	                        
                    </div>
                </div>
                <div class="col-md-8 md-col mb-4">
                    <h1 class="mb-3">${product.prodName}</h1>
                    <div class="row mb-3">
                        <div class="col">
                            <fmt:setLocale value="vi_VN"/>
                            <p class="dollar" style="float: left;"><span><fmt:formatNumber value="${product.price}" type="currency" /></span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="${pageContext.request.contextPath}/addToCart" >
                                <div class="form-group row">
                                    <input type="number" id="quantity" name="quantity" value="1" min="1" max="${product.quantity}" required/>
                                    <label class="col-sm-9 col-form-label">${product.quantity} products in stock!</label>
                                </div>
                                <input hidden name="id" value="${product.prodId}" />
                                <button style="cursor: pointer;" class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD TO CART</button>
                            </form>
                        </div>                        
                    </div>
                    
                </div>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th colspan="2"><h3>Specifications</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${product.manufacturer != ''}">
                        <tr>
                            <td>Manufacturer</td>
                            <td class="specifications">${product.manufacturer}</td>
                        </tr>
                    </c:if>
                    <c:if test="${product.size != ''}">
                    <tr>
                      <td>Size</td>
                      <td class="specifications">${product.size}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.weight != 0}">
                    <tr>
                      <td>Weight</td>
                      <td class="specifications">${product.weight}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.ram != ''}">
                    <tr>
                      <td>RAM</td>
                      <td class="specifications">${product.ram}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.rom != ''}">
                    <tr>
                      <td>ROM</td>
                      <td class="specifications">${product.rom}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.battery != ''}">
                    <tr>
                      <td>Battery</td>
                      <td class="specifications">${product.battery}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.operatingSystem != ''}">
                    <tr>
                      <td>Operating System</td>
                      <td class="specifications">${product.operatingSystem}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.sim != ''}">
                    <tr>
                      <td>SIM</td>
                      <td class="specifications">${product.sim}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.screen != ''}">
                    <tr>
                      <td>Screen</td>
                      <td class="specifications">${product.screen}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.cpu != ''}">
                    <tr>
                      <td>CPU</td>
                      <td class="specifications">${product.cpu}</td>
                    </tr>
                    </c:if>
                    <c:if test="${product.extSDcard != ''}">
                    <tr>
                      <td>Extend SD Card</td>
                      <td class="specifications">${product.extSDcard}</td>
                    </tr>
                    </c:if>                            
                    <c:if test="${product.releaseDate != ''}">
                    <tr>
                      <td>Release Date</td>
                      <td class="specifications">${product.releaseDate}</td>
                    </tr>
                    </c:if>
                </tbody>
            </table>
        </div>
                                
        <jsp:include page="/WEB-INF/views/_footer.jsp"></jsp:include>
    </body>
</html>
<script type="text/javascript">
    $('#quantity').number({
        'containerClass' :'number-style col-sm-3',
        'minus' :'number-minus',
        'plus' :'number-plus',
        'containerTag' :'div',
        'btnTag' :'span'
    });
</script>