<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

            <div class="container">
                    <h2 class="account-in">Login   </h2>
                    <p class="mb-3" style="color: red;">${errorString}</p>
                    <form method="POST" action="${pageContext.request.contextPath}/login?redirectUrl=${redirectUrl}" class="needs-validation" novalidate>
                        <div class="form-group row">
                          <label for="email" class="col-sm-1 col-form-label">Email</label>
                          <div class="col-sm-7">
                              <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="${user.email}" required="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="password" class="col-sm-1 col-form-label">Password</label>
                          <div class="col-sm-7">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="offset-sm-1 col-sm-7">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="rememberMe" name="rememberMe" value= "Y" >
                                <label class="custom-control-label" for="rememberMe">
                                    Remember me
                                  </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="offset-sm-1 col-sm-7">
                            <button type="submit" class="btn btn-default mr-3">Login</button>
                            <a href="${pageContext.request.contextPath}/register" style="color: #269abc">Register new account</a>
                          </div>
                        </div>
                      </form>
            </div>

        <jsp:include page="_footer.jsp"></jsp:include>
    </body>
</html>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
</script>