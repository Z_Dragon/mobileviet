<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Information Page</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

            <div class="container">
                <h1 class="mb-4">Information</h1>
                <p style="color: red;">${errorString}</p>                    
                <form method="POST" action="${pageContext.request.contextPath}/updateInfo" class="needs-validation" novalidate>
                    <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-7">
                          <input type="email" value="${user.email}" class="form-control" id="email" name="email" placeholder="Email" disabled="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="fullName" class="col-sm-2 col-form-label">Full Name</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" value="${user.fullName}" required="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="address" class="col-sm-2 col-form-label">Address</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="${user.address}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="${user.phone}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-7">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                      </div>
                    </div>
                  </form>
            </div>

        <jsp:include page="_footer.jsp"></jsp:include>
    </body>
</html>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
</script>