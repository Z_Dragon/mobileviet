<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order List</title>
    </head>
    <body>        
        <jsp:include page="/WEB-INF/views/_header.jsp"></jsp:include>
        <div class="container">
            <h1 class="mb-4">Orders</h1>
            <p style="color: red;">${errorString}</p>
            <div class="row">
                <div class="col-md-12 md-col mb-4">
                    <table id="oTable" class="table table-bordered table-hover">
                        <thead>
                            <th>Order Code</th>
                            <th>Date of Purchase</th>
                            <th>Delivery Address</th>
                            <th>Phone Contact</th>
                            <th>Total</th>
                            <th>Order status</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${orderList}" var="order" >
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/orderDetail?id=${order.orderId}" >#<fmt:formatNumber pattern="0000000" value="${order.orderId}" /></a></td>
                                    <td>
                                        <fmt:setLocale value="en_US"/>
                                        <fmt:formatDate pattern="MMM dd, yyyy HH:mm:ss" value="${order.createdDate}" />
                                    </td>
                                    <td>${order.address}</td>
                                    <td>${order.phone}</td>
                                    <td>
                                        <fmt:setLocale value="vi_VN"/>
                                        <fmt:formatNumber value="${order.total}" type="currency" />
                                    </td>
                                    <td>${order.statusName}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <jsp:include page="/WEB-INF/views/_footer.jsp"></jsp:include>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#oTable').DataTable( {
            order: [[ 0, 'desc' ]]
        } );
    } );
</script>