<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Detail</title>
    </head>
    <body>
        <jsp:include page="/WEB-INF/views/_header.jsp"></jsp:include>
        <div class="container mb-4">
            <h1 class="mb-3 d-inline-block" >Order #<fmt:formatNumber pattern="0000000" value="${order.orderId}" /> - ${order.statusName}</h1>
            <c:if test="${order.statusId == 0}">
                <button class="btn btn-danger float-right mt-1" onclick="comfirm_cancel(${order.orderId});">Cancel</button>
            </c:if>
            <div class="progress mb-3">
                <div class="progress-bar" role="progressbar" aria-valuenow="${order.statusId}" aria-valuemin="0" aria-valuemax="3" style="min-width: 2em; width: ${percentOrder}%;">
                  ${percentOrder}%
                </div>
            </div>
            <p style="color: red;" class="mb-3">${errorString}</p>
            <div class="row mb-4">
                <div class="col-sm-12">
                    <table id="odTable" class="table table-bordered table-hover">
                        <thead>
                            <th></th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${orderProductList}" var="product" >
                                <tr>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/productInfo?id=${product.prodId}">
                                            <img src="images/products/${product.prodImage}" alt="${product.prodName}" />
                                        </a>
                                    </td>
                                    <td>
                                        <a style="text-decoration: none;" href="${pageContext.request.contextPath}/productInfo?id=${product.prodId}">
                                            ${product.prodName}
                                        </a>
                                    </td>
                                    <td>
                                        <fmt:setLocale value="vi_VN"/>
                                        <p class="dollar"><span><fmt:formatNumber value="${product.price}" type="currency" /></span></p>
                                    </td>
                                    <td>${product.quantity}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Total</label>
                <div class="col-sm-7">
                    <fmt:setLocale value="vi_VN"/>
                    <strong class="form-control">
                        <fmt:formatNumber value="${total}" type="currency" />
                    </strong>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date of Purchase</label>
                <div class="col-sm-7">
                    <fmt:setLocale value="en_US"/>
                    <div class="form-control">
                        <fmt:formatDate pattern="MMM dd, yyyy HH:mm:ss" value="${order.createdDate}" />
                    </div>                    
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Delivery Address</label>
                <div class="col-sm-7">
                    <div class="form-control">${order.address}</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Phone Contact</label>
                <div class="col-sm-7">
                    <div class="form-control">${order.phone}</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Delivery Method</label>
                <div class="col-sm-7">
                    <div class="form-control">${order.deliveryMethod}</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Payment Method</label>
                <div class="col-sm-7">
                    <div class="form-control">${order.paymentMethod}</div>
                </div>
            </div>
        </div>

        <jsp:include page="/WEB-INF/views/_footer.jsp"></jsp:include>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#odTable').DataTable( {
            order: [[ 1, 'asc' ]]
        } );
    } );
    
    function comfirm_cancel(orderId){
        if(confirm("Do you want to cancel this order?"))
        {
            window.location = "<%=request.getContextPath()%>" + "/cancelOrder?id=" + orderId; 
        }else{
            return false;
        }
        return true;
    }
</script>