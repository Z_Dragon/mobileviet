<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Management</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>

                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Users</h1>                        
                    </div>
                    <table class="table table-bordered table-hover" id="ulTable">
                        <thead>
                            <th>User ID</th>
                            <th>Email</th>
                            <th>Full Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${userList}" var="user" >
                                <tr>
                                    <td>${user.userId}</td>                                    
                                    <td>${user.email}</td>
                                    <td>${user.fullName}</td>
                                    <td>${user.address}</td>
                                    <td>${user.phone}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </main>
            </div>
        </div>
    </body>
</html>
<jsp:include page="_footer.jsp"></jsp:include>
<script type="text/javascript">
    $(document).ready(function() {
        $('#ulTable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5'
            ],
            order: [[ 0, 'asc' ]]
        } );
    } );
</script>