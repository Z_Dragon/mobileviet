<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Detail</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>
        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>
                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="mb-1">Order #<fmt:formatNumber pattern="0000000" value="${order.orderId}" /> - ${order.statusName}</h1>
                    </div>
            
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar" aria-valuenow="${order.statusId}" aria-valuemin="0" aria-valuemax="3" style="min-width: 2em; width: ${percentOrder}%;">
                          ${percentOrder}%
                        </div>
                    </div>
                    <p class="mb-3" style="color: red;">${errorString}</p>
                    <div class="row">
                        <div class="col-md-9 md-col mb-4">
                            <table id="odTable" class="table table-bordered table-hover">
                                <thead>
                                    <th></th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                </thead>
                                <tbody>
                                    <c:forEach items="${orderProductList}" var="product" >
                                        <tr>
                                            <td><img src="images/products/${product.prodImage}" alt="${product.prodName}" /></td>
                                            <td>${product.prodName}</td>
                                            <td>
                                                <fmt:setLocale value="vi_VN"/>
                                                <p class="dollar"><span><fmt:formatNumber value="${product.price}" type="currency" /></span></p>
                                            </td>
                                            <td>${product.quantity}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3 md-col mb-4">
                            <form method="POST" action="${pageContext.request.contextPath}/adminUpdateStatusOrder?id=${order.orderId}">
                                <div class="form-group">
                                    <label for="orderStatus" class="col-sm-12 col-form-label">Order Status</label>
                                    <div class="col-sm-12">
                                        <select class="custom-select" id="orderStatus" name="orderStatus" value="${order.statusId}" <c:if test="${order.statusId == -1 || order.statusId == 3}">disabled</c:if>>
                                            <c:forEach items="${orderStatus}" var="item">
                                                <c:choose>
                                                    <c:when test="${item.id == order.statusId}">
                                                        <option value="${item.id}" selected>${item.text}</option>
                                                    </c:when>     
                                                    <c:otherwise>
                                                        <option value="${item.id}">${item.text}</option>
                                                    </c:otherwise>
                                                 </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <c:if test="${order.statusId != -1 && order.statusId != 3}">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary" >Update Status</button>
                                        </div>
                                    </div>
                                </c:if>
                            </form>
                        </div>
                    </div>
            
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Total</label>
                        <div class="col-sm-7">
                            <fmt:setLocale value="vi_VN"/>
                            <strong class="form-control">
                                <fmt:formatNumber value="${total}" type="currency" />
                            </strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Date of Purchase</label>
                        <div class="col-sm-7">
                            <fmt:setLocale value="en_US"/>
                            <div class="form-control">
                                <fmt:formatDate pattern="MMM dd, yyyy HH:mm:ss" value="${order.createdDate}" />
                            </div>                    
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Delivery Address</label>
                        <div class="col-sm-7">
                            <div class="form-control">${order.address}</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Phone Contact</label>
                        <div class="col-sm-7">
                            <div class="form-control">${order.phone}</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Delivery Method</label>
                        <div class="col-sm-7">
                            <div class="form-control">${order.deliveryMethod}</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Payment Method</label>
                        <div class="col-sm-7">
                            <div class="form-control">${order.paymentMethod}</div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </body>
</html>
<jsp:include page="_footer.jsp"></jsp:include>
<script type="text/javascript">
    $(document).ready(function() {
        $('#odTable').DataTable( {
            dom: 'Bfrtip',
            order: [[ 1, 'asc' ]],
            buttons: [
                'pdfHtml5'
            ]
        } );
    } );
</script>