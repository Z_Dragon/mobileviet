<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Side Navigation</title>
    </head>
    <body>
        <nav class="col-sm-2 d-none d-sm-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column ml-3">
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/adminOrder">
                            <span data-feather="file"></span>
                            Orders
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/adminProduct">
                            <span data-feather="shopping-cart"></span>
                            Products
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/adminUser">
                            <span data-feather="users"></span>
                            User
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </body>
    <footer>
        <!-- Icons -->
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
        <script>
          feather.replace();
        </script>
    </footer>
</html>
