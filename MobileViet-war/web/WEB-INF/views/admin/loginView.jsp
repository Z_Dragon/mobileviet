<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

        <title>Admin Login</title>
    </head>

    <body>
        <jsp:include page="_header.jsp"></jsp:include>

        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>

                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2 mb-3">Login</h1>                        
                    </div>
                    <p style="color: red;">${errorString}</p>
                    <form method="POST" action="${pageContext.request.contextPath}/adminLogin" class="needs-validation" novalidate>
                        <div class="form-group row">
                            <label for="email" class="col-sm-1 col-form-label">Email</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="${employee.email}" required>                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-1 col-form-label">Password</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-1 col-sm-1">
                                <button type="submit" class="btn btn-primary">Log in</button>
                            </div>
                        </div>
                    </form>
                </main>
            </div>
        </div>
    </body>
</html>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>