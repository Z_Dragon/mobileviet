<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product Management</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>

                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Products</h1>                        
                    </div>
                    <table class="table table-bordered table-hover" id="plTable">
                        <thead>
                            <th></th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>In Stock</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <c:forEach items="${productList}" var="product" >
                                <tr>
                                    <td><img src="images/products/${product.prodImage}" alt="${product.prodName}" /></td>                                    
                                    <td>${product.prodName}</td>
                                    <td>
                                        <fmt:setLocale value="vi_VN"/>
                                        <p class="dollar"><span><fmt:formatNumber value="${product.price}" type="currency" /></span></p>
                                    </td>
                                    <td>${product.quantity}</td>
                                    <td><a href="#" >Delete</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </main>
            </div>
        </div>
    </body>
</html>
<jsp:include page="_footer.jsp"></jsp:include>
<script type="text/javascript">
    $(document).ready(function() {
        $('#plTable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5'
            ],
            order: [[ 1, 'asc' ]]
        } );
    } );
</script>