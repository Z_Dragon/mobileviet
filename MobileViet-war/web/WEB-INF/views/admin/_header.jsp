<%@page import="models.Employee"%>
<%@page import="utils.MyUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

        <title>Header</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

        <!-- Bootstrap core CSS -->
        <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">
        
        <link href="css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/buttons.bootstrap4.css"/>
        <link rel="stylesheet" type="text/css" href="css/number.css"/>
    </head>

    <%
        Employee emp = MyUtils.getLoginedEmployee(request.getSession());    
        String fullName = emp == null ? "" : emp.getFullName();
    %>
  
    <body>
        <nav class="navbar navbar-expand-sm navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <a class="navbar-brand col-sm-2 mr-0 pl-4" href="${pageContext.request.contextPath}/admin">Mobile Viet</a>      
            <ul class="navbar-nav px-3">
                <% if( fullName == "" ) { %>
                    <li class="nav-item text-nowrap">
                        <a class="nav-link" href="${pageContext.request.contextPath}/adminLogin">Login</a>
                    </li>
                <% } else { %>
                    <li class="nav-item text-nowrap">
                        <a class="nav-link" href="${pageContext.request.contextPath}/empInfo">Welcome, <%out.print(fullName);%>!</a>
                    </li>
                    <li class="nav-item text-nowrap">
                        <a class="nav-link" href="${pageContext.request.contextPath}/logout?flag=admin">Logout</a>
                    </li>
                <% } %>
            </ul>
        </nav>
    </body>    
</html>
