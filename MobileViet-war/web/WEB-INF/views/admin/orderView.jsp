<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Management</title>
    </head>
    <body>        
        <jsp:include page="_header.jsp"></jsp:include>

        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>

                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Orders</h1>                        
                    </div>
                    <table id="olTable" class="table table-bordered table-hover">
                        <thead>
                            <th>Order Code</th>
                            <th>Date of Purchase</th>
                            <th>Customer</th>
                            <th>Delivery Address</th>
                            <th>Phone Contact</th>
                            <th>Total</th>
                            <th>Order status</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${orderList}" var="order" >
                                <tr>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/adminOrderDetail?id=${order.orderId}" >
                                            #<fmt:formatNumber pattern="0000000" value="${order.orderId}" />
                                        </a>
                                    </td>
                                    <td>
                                        <fmt:setLocale value="en_US"/>
                                        <fmt:formatDate pattern="MMM dd, yyyy HH:mm:ss" value="${order.createdDate}" />
                                    </td>
                                    <td>${order.userEmail}</td>
                                    <td>${order.address}</td>
                                    <td>${order.phone}</td>                                    
                                    <td>
                                        <fmt:setLocale value="vi_VN"/>
                                        <fmt:formatNumber value="${order.total}" type="currency" />
                                    </td>
                                    <td>${order.statusName}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </main>
            </div>
        </div>
    </body>
</html>
<jsp:include page="_footer.jsp"></jsp:include>
<script type="text/javascript">
    $(document).ready(function() {
        $('#olTable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5'
            ],
            order: [[ 0, 'desc' ]]
        } );
    } );
</script>