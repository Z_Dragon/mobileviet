<%@page import="models.Employee"%>
<%@page import="utils.MyUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

        <title>Admin Management</title>
    </head>
    <%
        Employee emp = MyUtils.getLoginedEmployee(request.getSession());    
        String fullName = emp == null ? "" : emp.getFullName();
    %>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

        <div class="container-fluid">
            <div class="row">
                <jsp:include page="_sideNavigation.jsp"></jsp:include>

                <main role="main" class="col-sm-10 ml-sm-auto pt-3 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">
                            <% if( fullName != "" ) { %>
                                Welcome, <%out.print(fullName);%>!
                            <% } %>
                        </h1>                        
                    </div>
                </main>
            </div>
        </div>
    </body>
</html>