<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart</title>
    </head>
    <body>
        <jsp:include page="/WEB-INF/views/_header.jsp"></jsp:include>
        <div class="container">
            <h1 class="mb-4">Cart</h1>
            <p class="mb-3" style="color: red;">${errorString}</p>
            <div class="row mb-4">
                <div class="col-md-9 col-sm-12">
                    <table id="cartTable" class="table table-bordered table-hover">
                        <thead>
                            <th></th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <c:forEach items="${cartProductList}" var="cartItem" >
                                <tr>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/productInfo?id=${cartItem.prodId}">
                                            <img src="images/products/${cartItem.prodImage}" alt="${cartItem.prodName}" />
                                        </a>
                                    </td>
                                    <td>
                                        <a style="text-decoration: none;" href="${pageContext.request.contextPath}/productInfo?id=${cartItem.prodId}">
                                            ${cartItem.prodName}
                                        </a>
                                    </td>
                                    <td style="min-width: 130px;">
                                        <fmt:setLocale value="vi_VN"/>
                                        <p class="dollar"><span><fmt:formatNumber value="${cartItem.price}" type="currency" /></span></p>
                                    </td>
                                    <td>                                        
                                        <form style="text-align: center;white-space: nowrap;" method="post" action="${pageContext.request.contextPath}/updateQuantityCart">
                                            <input class="quantity" style="" type="number" name="quantity" value="${cartItem.quantity}" min="1" max="${cartItem.instock}" required/>
                                            <input hidden name="id" value="${cartItem.cartId}" />
                                            <button type="submit" class="btn btn-primary mt-2">Update</button>
                                        </form>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger" href="${pageContext.request.contextPath}/deleteCartProduct?id=${cartItem.cartId}" >
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3 col-sm-12">
                    <form method="POST" action="${pageContext.request.contextPath}/checkOut" class="needs-validation" novalidate>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 ">Total</label>
                            <div class="col-sm-9">
                                <fmt:setLocale value="vi_VN"/>
                                <input type="text" class="form-control" value="<fmt:formatNumber value="${total}" type="currency" />" disabled="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 ">Delivery Address</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="${address}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 ">Phone Contact</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="${phone}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deliveryMethod" class="col-sm-3 ">Delivery Method</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="deliveryMethod" name="deliveryMethod" value="1">
                                    <c:forEach items="${deliveryMethod}" var="item">
                                        <option value="${item.id}">${item.text}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="paymentMethod" class="col-sm-3 ">Payment Method</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="paymentMethod" name="paymentMethod" value="1">
                                    <c:forEach items="${paymentMethod}" var="item">
                                        <option value="${item.id}">${item.text}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                          <div class="offset-sm-3 col-sm-9">
                            <button type="submit" class="btn btn-success">Check out</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
                                
        <jsp:include page="/WEB-INF/views/_footer.jsp"></jsp:include>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#cartTable').DataTable( {
            order: [[ 1, 'asc' ]]            
        } );
    } );
    
    $('.quantity').each(function (){
        $(this).number({
            'containerClass' :'number-style intable',
            'minus' :'number-minus',
            'plus' :'number-plus',
            'containerTag' :'div',
            'btnTag' :'span'
        });
    });
    
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
</script>