<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mobile Viet</title>        
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>
        
        <jsp:include page="_banner.jsp"></jsp:include>
        
        <jsp:include page="products/productListView.jsp"></jsp:include>
        
        <jsp:include page="_footer.jsp"></jsp:include>
    </body>
</html>