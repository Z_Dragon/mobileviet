<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
    </head>
    <body>
        <jsp:include page="_header.jsp"></jsp:include>

            <div class="container">
                    <h1 class="account-in mb-4">Register New Account</h1>
                    <p style="color: red;">${errorString}</p>                    
                    <form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/register">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email <span style="color: red;">*</span></label>
                          <div class="col-sm-10">
                              <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="password" class="col-sm-2 control-label">Password <span style="color: red;">*</span></label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="passwordConfirm" class="col-sm-2 control-label">Password Confirm <span style="color: red;">*</span></label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm" placeholder="Password Confirm" required="">
                          </div>
                        </div>
                        <div class="form-group">
                      <label for="fullName" class="col-sm-2 control-label">Full Name <span style="color: red;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" required="" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="address" class="col-sm-2 control-label">Address</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="phone" class="col-sm-2 control-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" >
                      </div>
                    </div>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Sign Up</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>

        <jsp:include page="_footer.jsp"></jsp:include>
    </body>
</html>
