<%@page import="models.UserAccount"%>
<%@page import="utils.MyUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
       
        <!--<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css" media="all" />-->
        
        <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/number.css" rel="stylesheet" type="text/css" media="all" />
        
        <!-- Custom Theme files -->
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="application/x-javascript">
            addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
        </script>
        <!--fonts-->
        <link href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <!--//fonts-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        
        <!--slider-script-->
        <script src="js/responsiveslides.min.js"></script>
        <script>
            $(function () {
                $("#slider1").responsiveSlides({
                    auto: true,
                    speed: 1000,
                    namespace: "callbacks",
                    pager: true,
                });
            });
        </script>
        <!--//slider-script-->
        <script>$(document).ready(function (c) {
                $('.alert-close').on('click', function (c) {
                    $('.message').fadeOut('slow', function (c) {
                        $('.message').remove();
                    });
                });
            });
        </script>
        <script>$(document).ready(function (c) {
                $('.alert-close1').on('click', function (c) {
                    $('.message1').fadeOut('slow', function (c) {
                        $('.message1').remove();
                    });
                });
            });
        </script>
    </head>
    
<%
    UserAccount user = MyUtils.getLoginedUser(request.getSession());    
    String fullName = user == null ? "" : user.getFullName();
%>

    <body>
        <!--header-->
        <div class="header mb-2">
            <div class="header-top">
                <div class="container">	
                    <div class="header-top-in">			
                        <div class="logo">
                            <a href="${pageContext.request.contextPath}/"><img src="images/logo.png" alt="Mobile Viet" ></a>
                        </div>
                        <div class="header-in">
                            <ul class="icon1 sub-icon1">
                                <li><a href="${pageContext.request.contextPath}/order" >Order</a></li>
                                <li><a href="${pageContext.request.contextPath}/cart" >Cart</a></li>
                                <% if( fullName == "" ) { %>
                                <li><p>Welcome, visitor!</p></li>
                                    <li><a href="${pageContext.request.contextPath}/login">Login</a></li>
                                    <li><a href="${pageContext.request.contextPath}/register">Sign up</a></li>
                                <% } else { %>
                                    <li><a href="${pageContext.request.contextPath}/userInfo">Welcome, <%out.print(fullName);%>!</a></li>
                                    <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                                <% } %>
                                <!--<li><div class="cart">
                                        <a href="#" class="cart-in"> </a>
                                        <span> 0</span>
                                    </div>
                                    <ul class="sub-icon1 list">
                                        <h3>Recently added items(2)</h3>
                                        <div class="shopping_cart">
                                            <div class="cart_box">
                                                <div class="message">
                                                    <div class="alert-close"> </div> 
                                                    <div class="list_img"><img src="images/14.jpg" class="img-responsive" alt=""></div>
                                                    <div class="list_desc"><h4><a href="#">velit esse molestie</a></h4>1 x<span class="actual">
                                                            $12.00</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="cart_box1">
                                                <div class="message1">
                                                    <div class="alert-close1"> </div> 
                                                    <div class="list_img"><img src="images/15.jpg" class="img-responsive" alt=""></div>
                                                    <div class="list_desc"><h4><a href="#">velit esse molestie</a></h4>1 x<span class="actual">
                                                            $12.00</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="total">
                                            <div class="total_left">CartSubtotal : </div>
                                            <div class="total_right">$250.00</div>
                                            <div class="clearfix"> </div>
                                        </div>
                                        <div class="login_buttons">
                                            <div class="check_button"><a href="checkout.html">Check out</a></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </ul>
                                </li>-->
                            </ul>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="h_menu4">
                        <!--<a class="toggleMenu" href="#">Menu</a>-->
                        <ul class="nav">
                            <li class="active"><a href="${pageContext.request.contextPath}/"><i> </i>Home</a></li>                            				
                            <!--<li><a href="products.html" >Apple</a></li>            
                            <li><a href="products.html" >SamSung</a></li>						  				 
                            <li><a href="products.html" >LG</a></li>
                            <li><a href="products.html" >Oppo</a></li>
                            <li><a href="products.html" >Sony</a></li>
                            <li><a href="products.html" >XiaoMi</a></li>
                            <li><a href="products.html" >Lenovo</a></li>-->
                        </ul>
                        <script type="text/javascript" src="js/nav.js"></script>
                    </div>
                </div>
            </div>
            <div class="header-bottom-in">
                <div class="container">
                    <div class="header-bottom-on">
                        <% if( fullName == "" ) { %>
                        <p class="wel"><a href="${pageContext.request.contextPath}/login">Welcome visitor you can login or create an account.</a></p>
                        <% } %>
                        <div class="header-can">
                            <!--<ul class="social-in">
                                <li><a href="#"><i class="facebook"> </i></a></li>
                            </ul>-->
                            <div class="search">
                                <form>
                                    <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                this.value = '';
                                            }" >
                                    <input type="submit" value="">
                                </form>
                            </div>

                            <div class="clearfix"> </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
