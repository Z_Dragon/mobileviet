<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>banner</title>
    </head>
    <body>
        <div class="banner-mat">
            <div class="container">
                <div class="banner">

                    <!-- Slideshow 4 -->
                    <div class="slider">
                        <ul class="rslides" id="slider1">
                            <li><img src="images/banner_Sony.jpg" alt="">
                            </li>
                            <li><img src="images/banner_LG.jpg" alt="">
                            </li>
                        </ul>
                    </div>

                    <div class="banner-bottom">
                        <div class="banner-matter">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>				
                <!-- //slider-->
            </div>
        </div>
    </body>
</html>
