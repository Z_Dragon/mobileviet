package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "CartServlet", urlPatterns = {"/cart"})
public class CartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedUser == null) {
            // Redirect (Chuyển hướng) tới trang login.
            response.sendRedirect(request.getContextPath() + "/login?redirectUrl=" + MyUtils.getFullURI(request));
            return;
        }
        
        Connection conn = MyUtils.getStoredConnection(request);
 
        String errorString = (String) request.getAttribute("errorString");
        List<CartItem> list = null;
        try {
            list = DBUtils.getCart(conn);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        int total = 0;
        for (CartItem cartItem : list) {
            total += cartItem.getPrice() * cartItem.getQuantity();
        }
        
        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("user", loginedUser);
        request.setAttribute("errorString", errorString);
        request.setAttribute("cartProductList", list);
        request.setAttribute("address", loginedUser.getAddress());
        request.setAttribute("phone", loginedUser.getPhone());
        request.setAttribute("total", total);
        request.setAttribute("deliveryMethod", XMLUtils.getAllStatus("delivery-method", request.getServletContext().getRealPath("")));
        request.setAttribute("paymentMethod", XMLUtils.getAllStatus("payment-method", request.getServletContext().getRealPath("")));
        // Nếu người dùng đã login thì forward (chuyển tiếp) tới trang
        // /WEB-INF/views/cartView.jsp
        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/cartView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
