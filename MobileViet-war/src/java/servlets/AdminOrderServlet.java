package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;


@WebServlet(name = "AdminOrderServlet", urlPatterns = {"/adminOrder"})
public class AdminOrderServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        Employee loginedEmp = MyUtils.getLoginedEmployee(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedEmp == null) {
            request.setAttribute("errorString", "You must login to manage orders!");

            response.sendRedirect(request.getContextPath() + "/adminLogin");
        } else {
            Connection conn = MyUtils.getStoredConnection(request);

            String errorString = null;
            List<Order> list = null;
            try {
                list = AdminDBUtils.getOrder(conn, request.getServletContext().getRealPath(""));
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
            // Lưu thông tin vào request attribute trước khi forward sang views.
            request.setAttribute("errorString", errorString);
            request.setAttribute("orderList", list);
            RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/orderView.jsp");

            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
