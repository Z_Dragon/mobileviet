package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "CancelOrderServlet", urlPatterns = {"/cancelOrder"})
public class CancelOrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = null;
        boolean hasError = false;
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedUser == null) {
            // Redirect (Chuyển hướng) tới trang login.
            response.sendRedirect(request.getContextPath() + "/login");
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            int orderId = Integer.parseInt(request.getParameter("id"));
            try {
                DBUtils.cancelOrder(conn, orderId, loginedUser.getEmail());
            } catch (SQLException e) {
                e.printStackTrace();
                //hasError = true;
                errorString = e.getMessage();
            }
                    
            response.sendRedirect(request.getContextPath() + "/orderDetail?id=" + orderId);
        }   
    }
}
