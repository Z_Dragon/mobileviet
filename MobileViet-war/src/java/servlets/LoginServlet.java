package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Forward tới trang /WEB-INF/views/loginView.jsp
        // (Người dùng không thể truy cập trực tiếp
        // vào các trang JSP đặt trong thư mục WEB-INF).
        request.setAttribute("redirectUrl", request.getParameter("redirectUrl"));
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/loginView.jsp");//this.getServletContext().getRequestDispatcher()
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String rememberMeStr = request.getParameter("rememberMe");
        boolean remember = "Y".equals(rememberMeStr);
        String redirectUrl = request.getParameter("redirectUrl");
 
        UserAccount user = null;
        boolean hasError = false;
        String errorString = null;
 
        Connection conn = MyUtils.getStoredConnection(request);
        try {
            // Tìm user trong DB.
            user = DBUtils.getUserInfo(conn, email);

            if (user == null) {
                hasError = true;
                errorString = "Email is invalid!";
            }
            else if (!user.getPassword().equals(PasswordUtils.getSecurePassword(password))) {
                hasError = true;
                errorString = "Password is invalid!";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        
        // Trong trường hợp có lỗi,
        // forward (chuyển hướng) tới /WEB-INF/views/login.jsp
        if (hasError) {
            user = new UserAccount();
            user.setEmail(email);
            //user.setPassword(password);
 
            // Lưu các thông tin vào request attribute trước khi forward.
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", user);
            request.setAttribute("redirectUrl", redirectUrl);

            RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
 
            dispatcher.forward(request, response);
        }
        // Trường hợp không có lỗi.
        // Lưu thông tin người dùng vào Session.
        else {
            HttpSession session = request.getSession();
            MyUtils.storeLoginedUser(session, user);
 
            // Nếu người dùng chọn tính năng "Remember me".
            if (remember) {
                MyUtils.storeUserCookie(response, user);
            }
            // Ngược lại xóa Cookie
            else {
                MyUtils.deleteUserCookie(response); 
            }
            
            if (redirectUrl.isEmpty()){
                response.sendRedirect(request.getContextPath() + "/home");
            }
            else {
                response.sendRedirect(redirectUrl);
            }
        }        
    }
}
