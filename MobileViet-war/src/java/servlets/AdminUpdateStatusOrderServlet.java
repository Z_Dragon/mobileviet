package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "AdminUpdateStatusOrderServlet", urlPatterns = {"/adminUpdateStatusOrder"})
public class AdminUpdateStatusOrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = null;
        boolean hasError = false;
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        Employee loginedEmp = MyUtils.getLoginedEmployee(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedEmp == null) {
            request.setAttribute("errorString", "You must login to manage orders!");

            response.sendRedirect(request.getContextPath() + "/adminLogin");
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            int orderId = Integer.parseInt(request.getParameter("id"));
            int status = Integer.parseInt(request.getParameter("orderStatus"));
            try {
                AdminDBUtils.updateStatusOrder(conn, orderId, status, loginedEmp.getEmail());
            } catch (SQLException e) {
                e.printStackTrace();
                //hasError = true;
                errorString = e.getMessage();
            }
                    
            response.sendRedirect(request.getContextPath() + "/adminOrderDetail?id=" + orderId);
        }   
    }
}
