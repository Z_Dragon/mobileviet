package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Forward tới trang /WEB-INF/views/registerView.jsp
        // (Người dùng không thể truy cập trực tiếp
        // vào các trang JSP đặt trong thư mục WEB-INF).
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String fullName = request.getParameter("fullName");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");

        Connection conn = MyUtils.getStoredConnection(request);
        UserAccount user = null;
        boolean hasError = false;
        String errorString = null;

        if (email == null || password == null || passwordConfirm == null|| email.length() == 0 || password.length() == 0 || passwordConfirm.length() == 0) {
            hasError = true;
            errorString = "Required email and password!";
        }
        else if (!password.equals(passwordConfirm)) {
            hasError = true;
            errorString = "Password and Password confirm must match!";
        }
        else {            
            try {
                // Tìm user trong DB.
                user = DBUtils.getUserInfo(conn, email);

                if (user != null) {
                    hasError = true;
                    errorString = "This email has been registered!";
                }
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        // Trong trường hợp có lỗi,
        // forward (chuyển hướng) tới /WEB-INF/views/registerView.jsp
        if (hasError) {
            // Lưu các thông tin vào request attribute trước khi forward.
            request.setAttribute("errorString", errorString);
            request.setAttribute("email", email);
            request.setAttribute("password", password);
            request.setAttribute("passwordConfirm", passwordConfirm);
            request.setAttribute("fullName", fullName);
            request.setAttribute("address", address);
            request.setAttribute("phone", phone);

            RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");

            dispatcher.forward(request, response);
        } 
        // Trường hợp không có lỗi.
        // Đăng kí user mới và lưu thông tin người dùng vào Session.
        // Sau đó chuyển hướng sang trang chủ.
        else {
            try {
                DBUtils.registerNewUser(conn, email, password, fullName, address, phone);
                user = DBUtils.getUserInfo(conn, email);
            } catch (SQLException ex) {
                Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            HttpSession session = request.getSession();
            MyUtils.storeLoginedUser(session, user);

            // Redirect (Chuyển hướng) sang trang chủ.
            response.sendRedirect(request.getContextPath() + "/");
        }
    }
}
