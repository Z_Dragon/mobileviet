package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "OrderDetailServlet", urlPatterns = {"/orderDetail"})
public class OrderDetailServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedUser == null) {
            // Redirect (Chuyển hướng) tới trang login.
            response.sendRedirect(request.getContextPath() + "/login?redirectUrl=" + MyUtils.getFullURI(request));
            return;
        }
        
        Connection conn = MyUtils.getStoredConnection(request);
        
        int orderId = Integer.parseInt(request.getParameter("id"));
        
        String errorString = null;
        List<OrderItem> list = null;
        Order order = null;
        try {
            order = DBUtils.getOrderDetail(conn, orderId, loginedUser.getUserId(), request.getServletContext().getRealPath(""));
            list = DBUtils.getOrderItems(conn, orderId, loginedUser.getUserId());
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        int total = 0;
        for (OrderItem orderItem : list) {
            total += orderItem.getPrice() * orderItem.getQuantity();
        }
        
        int percentOrder = order.getStatusId() < 0 ? 0 : order.getStatusId() * 100 / 3;
        
        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("user", loginedUser);
        request.setAttribute("errorString", errorString);
        request.setAttribute("order", order);
        request.setAttribute("percentOrder", percentOrder);
        request.setAttribute("orderProductList", list);
        request.setAttribute("total", total);
        request.setAttribute("address", loginedUser.getAddress());
        request.setAttribute("phone", loginedUser.getPhone());
        
        // Nếu người dùng đã login thì forward (chuyển tiếp) tới trang
        // /WEB-INF/views/cartView.jsp
        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/orders/orderDetailView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
