package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "CheckOutServlet", urlPatterns = {"/checkOut"})
public class CheckOutServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = null;
        boolean hasError = false;
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedUser == null) {
            request.setAttribute("errorString", "You must login to check out your cart!");
            response.sendRedirect(request.getContextPath() + "/login?redirectUrl=" + request.getContextPath() + "/cart");
            return;
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            int deliveryMethodId = Integer.parseInt(request.getParameter("deliveryMethod"));
            int paymentMethodId = Integer.parseInt(request.getParameter("paymentMethod"));
            
            try {
                List<CartItem> listCart = DBUtils.getCart(conn);
                Product prod = new Product();
                for (CartItem cartItem : listCart) {
                    prod = DBUtils.getProduct(conn, cartItem.getProdId());
                    
                    if (cartItem.getQuantity() > prod.getQuantity()){
                        hasError = true;
                        errorString = "There are only " + prod.getQuantity() + " " + prod.getProdName() + " in stock left!";
                        break;
                    }
                }
                if (hasError){
                    request.setAttribute("errorString", errorString);
                    RequestDispatcher dispatcher //
                        = request.getRequestDispatcher("/cart");
                    dispatcher.forward(request, response);                    
                }
                else {
                    DBUtils.checkOut(conn, loginedUser.getUserId(), address, phone, deliveryMethodId, paymentMethodId, loginedUser.getEmail());
                    int orderId = DBUtils.getOrderIdRecent(conn);
                    DBUtils.insertOrderDetail(conn, orderId, loginedUser.getEmail());
                    DBUtils.deleteCartAfterCheckOut(conn, loginedUser.getUserId());
                }
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
            
            response.sendRedirect(request.getContextPath() + "/order");
        }   
    }
}
