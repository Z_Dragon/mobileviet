package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "AddToCartServlet", urlPatterns = {"/addToCart"})
public class AddToCartServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedUser == null) {
            request.setAttribute("errorString", "You must login to add product to cart!");
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        else {
            Connection conn = MyUtils.getStoredConnection(request);
 
            String proId = (String) request.getParameter("id");
            int quantity = request.getParameter("quantity") != null ? Integer.parseInt(request.getParameter("quantity")) : 1;
            int userId = loginedUser.getUserId();
            String email = loginedUser.getEmail();
            String errorString = null;
 
            try {
                CartItem cartItem = DBUtils.getCartItem(conn, proId, userId);
                if (cartItem == null) {
                    DBUtils.insertProductToCart(conn, proId, userId, quantity, email);
                }
                else {
                    DBUtils.addQuantityInCart(conn, cartItem.getCartId(), quantity, email);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }

            request.setAttribute("errorString", errorString);

            response.sendRedirect(request.getContextPath() + "/cart");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
