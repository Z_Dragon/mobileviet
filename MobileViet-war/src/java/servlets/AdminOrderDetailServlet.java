package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utils.*;

@WebServlet(name = "AdminOrderDetailServlet", urlPatterns = {"/adminOrderDetail"})
public class AdminOrderDetailServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Kiểm tra người dùng đã đăng nhập (login) chưa.
        Employee loginedEmp = MyUtils.getLoginedEmployee(session);
 
        // Nếu chưa đăng nhập (login).
        if (loginedEmp == null) {
            request.setAttribute("errorString", "You must login to manage orders!");

            response.sendRedirect(request.getContextPath() + "/adminLogin");
            return;
        }
        
        Connection conn = MyUtils.getStoredConnection(request);
        
        int orderId = Integer.parseInt(request.getParameter("id"));
        
        String errorString = null;
        List<OrderItem> list = null;
        Order order = null;
        try {
            order = AdminDBUtils.getOrderDetail(conn, orderId, request.getServletContext().getRealPath(""));
            list = AdminDBUtils.getOrderItems(conn, orderId);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        int total = 0;
        for (OrderItem orderItem : list) {
            total += orderItem.getPrice() * orderItem.getQuantity();
        }
        
        List<DataItem> listDataItem = XMLUtils.getAllStatus("order", request.getServletContext().getRealPath(""));
        List<DataItem> listTemp = new ArrayList<>();
        int orderStatusId = order.getStatusId();
        for (DataItem dataItem : listDataItem) {
            int id = Integer.parseInt(dataItem.getId());
            if (id == orderStatusId || id == -1 || id == orderStatusId + 1){
                listTemp.add(dataItem);
            }
        }
        
        int percentOrder = order.getStatusId() < 0 ? 0 : order.getStatusId() * 100 / 3;
        
        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("order", order);
        request.setAttribute("percentOrder", percentOrder);
        request.setAttribute("orderProductList", list);
        request.setAttribute("total", total);
        request.setAttribute("orderStatus", listTemp);
        
        // Nếu người dùng đã login thì forward (chuyển tiếp) tới trang
        // /WEB-INF/views/cartView.jsp
        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/orderDetailView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
