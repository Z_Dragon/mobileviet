package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.*;

public class AdminDBUtils {
    public static Employee getEmployeeInfo(Connection conn, String email) throws SQLException {
        String sql = "Select e.empId, e.status, e.email, e.password, e.fullName, e.role, e.phone\n"
                + "FROM [Employee] e\n"
                + "WHERE e.email = ? and e.status <> -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, email);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            Employee emp = new Employee();
            emp.setEmpId(rs.getInt("empId"));
            emp.setStatus(rs.getInt("status"));
            emp.setRole(rs.getInt("role"));
            emp.setEmail(email);
            emp.setPassword(rs.getString("password"));
            emp.setFullName(rs.getString("fullName"));
            emp.setPhone(rs.getString("phone"));
            return emp;
        }
        return null;
    }
    
    public static List<Product> getAllProducts(Connection conn, String path) throws SQLException {
        String sql = "SELECT p.prodId, p.prodName, p.prodImage, p.price, p.quantity FROM Product p WHERE p.status >= 0";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Product> list = new ArrayList<>();
        while (rs.next()) {
            String id = rs.getString("prodId");
            String name = rs.getString("prodName");
            String image = rs.getString("prodImage");
            int price = rs.getInt("price");
            int quantity = rs.getInt("quantity");
            Product product = new Product();
            product.setProdId(id);
            product.setProdName(name);
            product.setProdImage(image);
            product.setPrice(price);
            product.setQuantity(quantity);
            list.add(product);
        }
        return list;
    }
    
    public static List<UserAccount> getAllUsers(Connection conn) throws SQLException {
        String sql = "SELECT u.userId, u.email, u.fullName, u.address, u.phone\n"
                + "FROM [User] u\n"
                + "WHERE u.status <> -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<UserAccount> list = new ArrayList<>();
        while (rs.next()) {
            UserAccount user = new UserAccount();
            user.setUserId(rs.getInt("userId"));
            user.setEmail(rs.getString("email"));
            user.setFullName(rs.getString("fullName"));
            user.setAddress(rs.getString("address"));
            user.setPhone(rs.getString("phone"));
            list.add(user);
        }
        return list;
    }
    
    public static List<Order> getOrder(Connection conn, String path) throws SQLException {
        String sql = "SELECT o.[orderId], o.[userId], o.[address], o.[phone], o.[createdDate], o.[status], u.[email], SUM(ISNULL(od.quantity, 0) * ISNULL(p.price, 0)) AS total\n" +
                        "FROM [Order] o\n" +
                        "INNER JOIN dbo.[User] u ON u.userId = o.userId\n" +
                        "LEFT JOIN dbo.OrderDetail od ON od.orderId = o.orderId\n" +
                        "LEFT JOIN dbo.Product p ON p.prodId = od.prodId\n" +
                        "WHERE o.status > -2\n" +
                        "GROUP BY o.[orderId], o.[userId], o.[address], o.[phone], o.[createdDate], o.[status], u.[email]\n" +
                        "ORDER BY o.[createdDate] DESC";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        ResultSet rs = pstm.executeQuery();
        List<Order> list = new ArrayList<>();
        while (rs.next()) {
            Order order = new Order();
            order.setOrderId(rs.getInt("orderId"));
            order.setUserId(rs.getInt("userId"));
            order.setUserEmail(rs.getString("email"));
            order.setAddress(rs.getString("address"));
            order.setPhone(rs.getString("phone"));
            order.setCreatedDate(rs.getTimestamp("createdDate"));
            order.setTotal(rs.getInt("total"));
            order.setStatusName(XMLUtils.getStatusName("order", rs.getInt("status"), path));
            list.add(order);
        }
        return list;
    }
    
    public static List<OrderItem> getOrderItems(Connection conn, int orderId) throws SQLException {
        String sql = "SELECT od.orderDetailId, p.prodId, p.prodName, p.prodImage, p.price, od.quantity\n" +
                        "FROM dbo.OrderDetail od\n" +
                        "INNER JOIN dbo.[Order] o ON o.orderId = od.orderId\n" +
                        "JOIN dbo.[User] u ON u.userId = o.userId\n" +
                        "JOIN dbo.Product p ON p.prodId = od.prodId\n" +
                        "WHERE o.orderId = ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, orderId);
        
        ResultSet rs = pstm.executeQuery();
        List<OrderItem> list = new ArrayList<>();
        while (rs.next()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderDetailId(rs.getInt("orderDetailId"));
            orderItem.setProdId(rs.getString("prodId"));
            orderItem.setProdName(rs.getString("prodName"));
            orderItem.setProdImage(rs.getString("prodImage"));            
            orderItem.setPrice(rs.getInt("price"));
            orderItem.setQuantity(rs.getInt("quantity"));
            list.add(orderItem);
        }
        return list;
    }
    
    public static Order getOrderDetail(Connection conn, int orderId, String path) throws SQLException {
        String sql = "SELECT o.[orderId], o.[userId], o.[status], o.[address], o.[phone], o.[deliveryMethod], o.[paymentMethod], o.[createdDate]\n" +
                    "FROM dbo.[Order] o\n" +
                    "WHERE o.orderId = ? AND o.status > -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, orderId);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            Order order = new Order();
            order.setOrderId(orderId);
            order.setUserId(rs.getInt("userId"));
            order.setStatusId(rs.getInt("status"));
            order.setStatusName(XMLUtils.getStatusName("order", rs.getInt("status"), path));
            order.setAddress(rs.getString("address"));
            order.setPhone(rs.getString("phone"));
            order.setCreatedDate(rs.getTimestamp("createdDate"));
            order.setDeliveryMethodId(rs.getInt("deliveryMethod"));
            order.setDeliveryMethod(XMLUtils.getStatusName("delivery-method", rs.getInt("deliveryMethod"), path));
            order.setPaymentMethodId(rs.getInt("paymentMethod"));
            order.setPaymentMethod(XMLUtils.getStatusName("payment-method", rs.getInt("paymentMethod"), path));
            return order;
        }
        return null;
    }
    
    public static void updateStatusOrder(Connection conn, int orderId, int status, String email) throws SQLException {
        String sql = "UPDATE [dbo].[Order]\n" +
                    "SET [status] = ?      \n" +
                    "   ,[modifiedBy] = ?\n" +
                    "   ,[modifiedDate] = ?\n" +
                    "WHERE orderId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, status);
        pstm.setString(2, email);
        pstm.setTimestamp(3, new Timestamp(new Date().getTime()));
        pstm.setInt(4, orderId);
 
        pstm.executeUpdate(); 
    }
}
