package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.*;

public class DBUtils {
    public static void registerNewUser(Connection conn, String email, String password, String fullName, String address, String phone) throws SQLException {
        String sql = "INSERT INTO dbo.[User] (email, password, fullName, address, phone, createdBy)\n" +
                    "VALUES(?, ?, ?, ?, ?, ?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, email);
        pstm.setString(2, PasswordUtils.getSecurePassword(password));
        pstm.setString(3, fullName);
        pstm.setString(4, address);
        pstm.setString(5, phone);
        pstm.setString(6, email);
 
        pstm.executeUpdate(); 
    }
    
    public static UserAccount getUserInfo(Connection conn, String email) throws SQLException {
        String sql = "Select a.userId, a.status, a.email, a.password, a.fullName, a.address, a.phone from [User] a "
                + " where a.email = ? and a.status <> -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, email);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            int userId = rs.getInt("userId");
            int status = rs.getInt("status");
            String emailDB = rs.getString("email");
            String password = rs.getString("password");
            String fullName = rs.getString("fullName");
            String address = rs.getString("address");
            String phone = rs.getString("phone");
            UserAccount user = new UserAccount(userId, status, emailDB, password, fullName, address, phone);
            return user;
        }
        return null;
    }
    
    public static void updateUserInfo(Connection conn, int userId, String fullName, String address, String phone, String email) throws SQLException {
        String sql = "UPDATE [dbo].[User]\n" +
                    "SET [fullName] = ?\n" +
                    "    ,[address] = ?\n" +
                    "    ,[phone] = ?\n" +
                    "    ,[modifiedBy] = ?\n" +
                    "    ,[modifiedDate] = ?\n" +
                    "WHERE userId = ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, fullName);
        pstm.setString(2, address);
        pstm.setString(3, phone);
        pstm.setString(4, email);
        pstm.setTimestamp(5, new Timestamp(new Date().getTime()));
        pstm.setInt(6, userId);
 
        pstm.executeUpdate(); 
    }

    public static List<Product> getAllProductsByKeyword(Connection conn) throws SQLException {
        List<Product> list = new ArrayList<>();
        list = getAllProductsByKeyword(conn, null);
        return list;
    }
    
    public static List<Product> getAllProductsByKeyword(Connection conn, String keyword) throws SQLException {
        String sql = "SELECT p.prodId, p.prodName, p.prodImage, p.price FROM Product p WHERE p.status >= 0";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Product> list = new ArrayList<>();
        while (rs.next()) {
            String id = rs.getString("prodId");
            String name = rs.getString("prodName");
            String image = rs.getString("prodImage");
            int price = rs.getInt("price");
            Product product = new Product();
            product.setProdId(id);
            product.setProdName(name);
            product.setProdImage(image);
            product.setPrice(price);
            list.add(product);
        }
        return list;
    }
 
    public static Product getProduct(Connection conn, String prodId) throws SQLException {
        String sql = "SELECT p.prodName, p.prodImage, p.price, p.manufacturer, p.size, p.ram, p.rom, p.screen, p.cpu, " +
                    "p.extSDcard, p.sim, p.battery, p.operatingSystem, p.weight, p.releaseDate, p.note, p.status, p.quantity\n" +
                    "FROM dbo.Product p \n" +
                    "WHERE p.prodId = ? AND p.status >= 0";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, prodId);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            Product product = new Product(prodId, rs.getString("prodName"), rs.getString("prodImage"), rs.getString("manufacturer"), rs.getString("size")
                                    , rs.getString("ram"), rs.getString("rom"), rs.getString("screen"), rs.getString("cpu"), rs.getString("extSDcard"), rs.getString("sim")
                                    , rs.getString("battery"), rs.getString("operatingSystem"), rs.getString("releaseDate"), rs.getString("note"), rs.getInt("price"), rs.getInt("status"), rs.getDouble("weight"));
            product.setQuantity(rs.getInt("quantity"));
            return product;
        }
        return null;
    }
    
    public static List<CartItem> getCart(Connection conn) throws SQLException {
        String sql = "SELECT c.cartId, p.prodId, p.prodName, p.prodImage, p.price, c.quantity, p.quantity as instock\n" +
                    "FROM dbo.Cart c\n" +
                    "LEFT JOIN dbo.Product p ON p.prodId = c.prodId";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<CartItem> list = new ArrayList<>();
        while (rs.next()) {
            CartItem cartItem = new CartItem();
            cartItem.setCartId(rs.getInt("cartId"));
            cartItem.setProdId(rs.getString("prodId"));
            cartItem.setProdName(rs.getString("prodName"));
            cartItem.setProdImage(rs.getString("prodImage"));
            cartItem.setPrice(rs.getInt("price"));
            cartItem.setQuantity(rs.getInt("quantity"));
            cartItem.setInstock(rs.getInt("instock"));
            list.add(cartItem);
        }
        return list;
    }
    
    public static void insertProductToCart(Connection conn, String prodId, int userId, int quantity, String email) throws SQLException {
        String sql = "INSERT INTO [dbo].[Cart] ([userId],[prodId],[quantity],[createdBy])\n" +
                        "VALUES(?, ?, ?, ?)";        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, userId);
        pstm.setString(2, prodId);
        pstm.setInt(3, quantity);
        pstm.setString(4, email);
 
        pstm.executeUpdate();
    }
    
    public static CartItem getCartItem(Connection conn, String prodId, int userId) throws SQLException {
        String sql = "Select c.cartId, c.quantity\n"
                + "FROM [dbo].[Cart] c\n"
                + "WHERE c.userId = ? AND c.prodId = ? AND c.status <> -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, userId);
        pstm.setString(2, prodId);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            CartItem cartItem = new CartItem();
            cartItem.setCartId(rs.getInt("cartId"));
            cartItem.setQuantity(rs.getInt("quantity"));
            return cartItem;
        }
        return null;
    }
    
    public static void addQuantityInCart(Connection conn, int cartId, int quantity, String email) throws SQLException {
        String sql = "UPDATE [dbo].[Cart]\n" +
                    "SET [quantity] = [quantity] + ?      \n" +
                    "   ,[modifiedBy] = ?\n" +
                    "   ,[modifiedDate] = ?\n" +
                    "WHERE cartId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, quantity);
        pstm.setString(2, email);
        pstm.setTimestamp(3, new Timestamp(new Date().getTime()));
        pstm.setInt(4, cartId);
 
        pstm.executeUpdate(); 
    }
    
    public static void updateQuantityInCart(Connection conn, int cartId, int quantity, String email) throws SQLException {
        String sql = "UPDATE [dbo].[Cart]\n" +
                    "SET [quantity] = ?      \n" +
                    "   ,[modifiedBy] = ?\n" +
                    "   ,[modifiedDate] = ?\n" +
                    "WHERE cartId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, quantity);
        pstm.setString(2, email);
        pstm.setTimestamp(3, new Timestamp(new Date().getTime()));
        pstm.setInt(4, cartId);
 
        pstm.executeUpdate(); 
    }
    
    public static void deleteProductFromCart(Connection conn, int cartId) throws SQLException {
        String sql = "DELETE FROM [dbo].[Cart]\n" +
                        "WHERE cartId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, cartId);
 
        pstm.executeUpdate();
    }
    
    public static void checkOut(Connection conn, int userId, String address, String phone, int deliveryMethodId, int paymentMethodId, String email) throws SQLException{
        String sql = "INSERT INTO [dbo].[Order] ([userId],[address],[phone],[deliveryMethod],[paymentMethod],[createdBy])\n" +
                        "VALUES(?, ?, ?, ?, ?, ?)";
                
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, userId);
        pstm.setString(2, address);
        pstm.setString(3, phone);
        pstm.setInt(4, deliveryMethodId);
        pstm.setInt(5, paymentMethodId);
        pstm.setString(6, email);
        pstm.executeUpdate();
    }
    
    public static int getOrderIdRecent(Connection conn) throws SQLException {
        int orderId;
        String sql = "SELECT TOP (1) [orderId]\n" +
        "FROM [dbo].[Order]\n" +
        "ORDER BY [createdDate] DESC";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            orderId = rs.getInt("orderId");
            return orderId;
        }
        return 0;
    }
    
    public static void insertOrderDetail(Connection conn, int orderId, String email) throws SQLException {
        String sql = "INSERT INTO [dbo].[OrderDetail] ([orderId],[prodId],[quantity],[createdBy])\n" +
                            "VALUES(?, ?, ?, ?)\n"
                + "UPDATE [dbo].[Product]\n" +
                    "SET [quantity] = [quantity] - ?\n" +
                    "WHERE prodId = ?";
        PreparedStatement pstm;
        if (orderId != 0){
            List<CartItem> cart = getCart(conn);
            for (CartItem cartItem : cart) {
                
                pstm = conn.prepareStatement(sql);

                pstm.setInt(1, orderId);
                pstm.setString(2, cartItem.getProdId());
                pstm.setInt(3, cartItem.getQuantity());
                pstm.setString(4, email);
                pstm.setInt(5, cartItem.getQuantity());
                pstm.setString(6, cartItem.getProdId());
                
                pstm.executeUpdate();                
            }
        }
    }
    
    public static void deleteCartAfterCheckOut(Connection conn, int userId) throws SQLException{
        String sql = "DELETE FROM [dbo].[Cart]\n" +
                        "WHERE userId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, userId);
 
        pstm.executeUpdate(); 
    }
    
    public static List<Order> getOrder(Connection conn, int userId, String path) throws SQLException {        
        String sql = "SELECT o.[orderId], o.[userId], o.[status], o.[address], o.[phone], o.[createdDate], SUM(ISNULL(od.quantity, 0) * ISNULL(p.price, 0)) AS total\n" +
                    "FROM [Order] o\n" +
                    "LEFT JOIN dbo.OrderDetail od ON od.orderId = o.orderId\n" +
                    "LEFT JOIN dbo.Product p ON p.prodId = od.prodId\n" +
                    "WHERE [userId] = ? AND o.status > -2\n" +
                    "GROUP BY o.[orderId], o.[userId], o.[status], o.[address], o.[phone], o.[createdDate]\n" +
                    "ORDER BY [createdDate] DESC";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, userId);
        
        ResultSet rs = pstm.executeQuery();
        List<Order> list = new ArrayList<>();
        while (rs.next()) {
            Order order = new Order();
            order.setOrderId(rs.getInt("orderId"));
            order.setUserId(rs.getInt("userId"));
            order.setAddress(rs.getString("address"));
            order.setPhone(rs.getString("phone"));            
            order.setCreatedDate(rs.getTimestamp("createdDate"));
            order.setStatusId(rs.getInt("status"));
            order.setStatusName(XMLUtils.getStatusName("order", rs.getInt("status"), path));
            order.setTotal(rs.getInt("total"));
            //order.setDeliveryMethodId(rs.getInt("deliveryMethod"));
            //order.setDeliveryMethod(XMLUtils.getStatusName("delivery-method", rs.getInt("deliveryMethod"), path));
            //order.setPaymentMethodId(rs.getInt("paymentMethod"));
            //order.setPaymentMethod(XMLUtils.getStatusName("payment-method", rs.getInt("paymentMethod"), path));
            list.add(order);
        }
        return list;
    }
    
    public static List<OrderItem> getOrderItems(Connection conn, int orderId, int userId) throws SQLException {
        String sql = "SELECT od.orderDetailId, p.prodId, p.prodName, p.prodImage, p.price, od.quantity\n" +
                        "FROM dbo.OrderDetail od\n" +
                        "INNER JOIN dbo.[Order] o ON o.orderId = od.orderId\n" +
                        "JOIN dbo.[User] u ON u.userId = o.userId\n" +
                        "JOIN dbo.Product p ON p.prodId = od.prodId\n" +
                        "WHERE o.orderId = ? AND u.userId = ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, orderId);
        pstm.setInt(2, userId);
        
        ResultSet rs = pstm.executeQuery();
        List<OrderItem> list = new ArrayList<>();
        while (rs.next()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderDetailId(rs.getInt("orderDetailId"));
            orderItem.setProdId(rs.getString("prodId"));
            orderItem.setProdName(rs.getString("prodName"));
            orderItem.setProdImage(rs.getString("prodImage"));            
            orderItem.setPrice(rs.getInt("price"));
            orderItem.setQuantity(rs.getInt("quantity"));
            list.add(orderItem);
        }
        return list;
    }
    
    public static Order getOrderDetail(Connection conn, int orderId, int userId, String path) throws SQLException {
        String sql = "SELECT o.[orderId], o.[userId], o.[status], o.[address], o.[phone], o.[deliveryMethod], o.[paymentMethod], o.[createdDate]\n" +
                    "FROM dbo.[Order] o\n" +
                    "WHERE o.orderId = ? AND o.userId = ? AND o.status > -2";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, orderId);
        pstm.setInt(2, userId);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            Order order = new Order();
            order.setOrderId(orderId);
            order.setUserId(userId);
            order.setStatusId(rs.getInt("status"));
            order.setStatusName(XMLUtils.getStatusName("order", rs.getInt("status"), path));
            order.setAddress(rs.getString("address"));
            order.setPhone(rs.getString("phone"));
            order.setCreatedDate(rs.getTimestamp("createdDate"));
            order.setDeliveryMethodId(rs.getInt("deliveryMethod"));
            order.setDeliveryMethod(XMLUtils.getStatusName("delivery-method", rs.getInt("deliveryMethod"), path));
            order.setPaymentMethodId(rs.getInt("paymentMethod"));
            order.setPaymentMethod(XMLUtils.getStatusName("payment-method", rs.getInt("paymentMethod"), path));
            return order;
        }
        return null;
    }
    
    public static void cancelOrder(Connection conn, int orderId, String email) throws SQLException {
        String sql = "UPDATE [dbo].[Order]\n" +
                    "SET [status] = ?\n" +
                    "   ,[modifiedBy] = ?\n" +
                    "   ,[modifiedDate] = ?\n" +
                    "WHERE orderId = ?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        
        pstm.setInt(1, -1);
        pstm.setString(2, email);
        pstm.setTimestamp(3, new Timestamp(new Date().getTime()));
        pstm.setInt(4, orderId);
 
        pstm.executeUpdate(); 
    }
}