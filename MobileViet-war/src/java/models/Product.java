package models;

/**
 *
 * @author Zet
 */
public class Product {
    private String prodId, prodName, prodImage, manufacturer, size, ram, rom, screen, cpu, extSDcard, sim, battery, operatingSystem, releaseDate, note;
    private int price, quantity, status;
    private double weight;

    public Product() {
 
    }

    public Product(String prodId, String prodName, String prodImage, String manufacturer, String size, String ram, String rom, String screen, String cpu, String extSDcard, String sim, String battery, String operatingSystem, String releaseDate, String note, int price, int status, double weight) {
        this.prodId = prodId;
        this.prodName = prodName;
        this.prodImage = prodImage;
        this.manufacturer = manufacturer;
        this.size = size;
        this.ram = ram;
        this.rom = rom;
        this.screen = screen;
        this.cpu = cpu;
        this.extSDcard = extSDcard;
        this.sim = sim;
        this.battery = battery;
        this.operatingSystem = operatingSystem;
        this.releaseDate = releaseDate;
        this.note = note;
        this.price = price;
        this.status = status;
        this.weight = weight;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
    
    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getRom() {
        return rom;
    }

    public void setRom(String rom) {
        this.rom = rom;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getExtSDcard() {
        return extSDcard;
    }

    public void setExtSDcard(String extSDcard) {
        this.extSDcard = extSDcard;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
