package models;

public class CartItem {
    private int cartId, price, quantity, instock;
    private String prodId, prodName, prodImage;

    public CartItem() {
    }

    public CartItem(int cartId, int price, int quantity, String prodName, String prodImage, String prodId) {
        this.cartId = cartId;
        this.price = price;
        this.quantity = quantity;
        this.prodName = prodName;
        this.prodImage = prodImage;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public int getInstock() {
        return instock;
    }

    public void setInstock(int instock) {
        this.instock = instock;
    }
}
