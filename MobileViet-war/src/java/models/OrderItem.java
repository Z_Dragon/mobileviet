package models;

public class OrderItem {
    private int orderDetailId, price, quantity;
    private String prodId, prodName, prodImage;

    public OrderItem() {
    }

    public OrderItem(int orderDetailId, int price, int quantity, String prodId, String prodName, String prodImage) {
        this.orderDetailId = orderDetailId;
        this.price = price;
        this.quantity = quantity;
        this.prodId = prodId;
        this.prodName = prodName;
        this.prodImage = prodImage;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }
}
